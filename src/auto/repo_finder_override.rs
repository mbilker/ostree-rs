// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use glib::object::IsA;
use glib::translate::*;
use ostree_sys;
use std::fmt;
use RepoFinder;

glib_wrapper! {
    pub struct RepoFinderOverride(Object<ostree_sys::OstreeRepoFinderOverride, ostree_sys::OstreeRepoFinderOverrideClass, RepoFinderOverrideClass>) @implements RepoFinder;

    match fn {
        get_type => || ostree_sys::ostree_repo_finder_override_get_type(),
    }
}

impl RepoFinderOverride {
    #[cfg(any(feature = "v2018_6", feature = "dox"))]
    pub fn new() -> RepoFinderOverride {
        unsafe {
            from_glib_full(ostree_sys::ostree_repo_finder_override_new())
        }
    }
}

#[cfg(any(feature = "v2018_6", feature = "dox"))]
impl Default for RepoFinderOverride {
    fn default() -> Self {
        Self::new()
    }
}

pub const NONE_REPO_FINDER_OVERRIDE: Option<&RepoFinderOverride> = None;

pub trait RepoFinderOverrideExt: 'static {
    #[cfg(any(feature = "v2018_6", feature = "dox"))]
    fn add_uri(&self, uri: &str);
}

impl<O: IsA<RepoFinderOverride>> RepoFinderOverrideExt for O {
    #[cfg(any(feature = "v2018_6", feature = "dox"))]
    fn add_uri(&self, uri: &str) {
        unsafe {
            ostree_sys::ostree_repo_finder_override_add_uri(self.as_ref().to_glib_none().0, uri.to_glib_none().0);
        }
    }
}

impl fmt::Display for RepoFinderOverride {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "RepoFinderOverride")
    }
}
